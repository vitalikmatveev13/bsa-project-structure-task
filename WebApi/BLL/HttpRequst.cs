﻿using BLL.Dto;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    static internal class HttpRequst
    {
        static readonly HttpClient client = new HttpClient();
        private static readonly string route = "https://bsa-dotnet.azurewebsites.net/";

        public async static Task<List<UserDto>> GetAllUsers()
        {
            var response = await client.GetAsync(route + "api/Users");
            var responseBody = await response.Content.ReadAsStringAsync();
            var users = JsonConvert.DeserializeObject<List<UserDto>>(responseBody);
            return users;
        }

        public async static Task<List<TeamDto>> GetAllTeams()
        {
            var response = await client.GetAsync(route + "api/Teams");
            var responseBody = await response.Content.ReadAsStringAsync();
            var teams = JsonConvert.DeserializeObject<List<TeamDto>>(responseBody);
            return teams;
        }

        public async static Task<List<ProjectDto>> GetAllProjects()
        {
            var response = await client.GetAsync(route + "api/Projects");
            var responseBody = await response.Content.ReadAsStringAsync();
            var projects = JsonConvert.DeserializeObject<List<ProjectDto>>(responseBody);
            return projects;
        }

        public async static Task<List<Dto.TaskDto>> GetAllTasks()
        {
            var response = await client.GetAsync(route + "api/Tasks");
            var responseBody = await response.Content.ReadAsStringAsync();
            var tasks = JsonConvert.DeserializeObject<List<Dto.TaskDto>>(responseBody);
            return tasks;
        }

    }
}
