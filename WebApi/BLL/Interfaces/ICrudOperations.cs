﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface ICrudOperations<T> where T : class
    {
        public Task CreateAsync(T entity);
        public Task UpdateAsync(T entity);
        public Task DeleteAsync(int entityId);
        public Task<T> GetAsync(int id);
        public IEnumerable<T> GetAll();
    }
}
