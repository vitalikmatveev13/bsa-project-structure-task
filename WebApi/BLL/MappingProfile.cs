﻿using AutoMapper;
using BLL.Dto;
using DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task = DAL.Model.Task;

namespace BLL
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<User,UserDto>().ReverseMap();
            CreateMap<Task,TaskDto>().ForMember(p=>p.Name, x=>x.MapFrom(x=>x.TaskName)).ReverseMap();
            CreateMap<Team,TeamDto>().ReverseMap();
            CreateMap<Project,ProjectDto>().ForMember(p => p.Name, x => x.MapFrom(x => x.ProjectName)).ReverseMap();
        }
    }
}
