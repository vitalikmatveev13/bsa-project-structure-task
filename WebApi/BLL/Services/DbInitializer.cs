﻿using DAL.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class DbInitializer : IDbInitializer
    {
        private readonly IServiceScopeFactory _scopeFactory;

        public DbInitializer(IServiceScopeFactory scopeFactory)
        {
            _scopeFactory = scopeFactory;
        }
        public void Initialize()
        {
            using (var serviceScope = _scopeFactory.CreateScope())
            {
                using (var context = serviceScope.ServiceProvider.GetService<AppDbContext>())
                {
                    context.Database.Migrate();
                }
            }
        }

        public void SeedData()
        {
            using (var serviceScope = _scopeFactory.CreateScope())
            {
                using (var context = serviceScope.ServiceProvider.GetService<AppDbContext>())
                {
                    if (!context.User.Any())
                    {
                        var user = new User
                        {
                            Email = "admin@gmail.com",
                            BirthDay = DateTime.Now,
                            FirstName = "Vitalii",
                            LastName = "Matvieiev",
                            RegisteredAt = DateTime.Now,
                        };
                        context.User.Add(user);
                    }

                    if (!context.Team.Any())
                    {
                        var team = new Team
                        {
                            Name = "MyTeam",
                            CreatedAt = DateTime.Now,
                        };
                        context.Team.Add(team);
                    }

                    if (!context.Project.Any())
                    {
                        context.Project.Add(new Project
                        {
                            ProjectName = "FirstProject",
                            CreatedAt = DateTime.Now,
                            Deadline = DateTime.MaxValue,
                            Description = "My new project",
                            AuthorId = null,
                        });
                    }

                    context.SaveChanges();
                }
            }
        }
    }
}
