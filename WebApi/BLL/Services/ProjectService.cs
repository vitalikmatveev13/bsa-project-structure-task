﻿using AutoMapper;
using BLL.Dto;
using BLL.Interfaces;
using DAL.Model;
using DAL.UoW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tasks = System.Threading.Tasks;

namespace BLL.Services
{
    public class ProjectService : IProjectService
    {
        private readonly UnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public ProjectService(UnitOfWork unitOfWork,
            IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Tasks.Task CreateAsync(ProjectDto entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException();
            }

            var project = _mapper.Map<Project>(entity);
            await _unitOfWork.Project.CreateAsync(project);
            await _unitOfWork.SaveChangesAsync();

        }

        public async Tasks.Task DeleteAsync(int entityId)
        {
            _unitOfWork.Project.Delete(entityId);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Tasks.Task UpdateAsync(ProjectDto entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException();
            }

            var project = _mapper.Map<Project>(entity);

            _unitOfWork.Project.Update(project);
            await _unitOfWork.SaveChangesAsync();
        }

        public IEnumerable<ProjectDto> GetAll()
        {
            var projectList = _mapper.Map<IEnumerable<ProjectDto>>(_unitOfWork.Project.GetAll());
            return projectList;
        }

        public async Task<ProjectDto> GetAsync(int id)
        {
            var project = await _unitOfWork.Project.FirstOrDefaultAsync(t => t.Id == id);

            var projectDto = _mapper.Map<ProjectDto>(project);
            
            return projectDto;
        }
    }
}
