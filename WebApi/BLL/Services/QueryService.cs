﻿using AutoMapper;
using BLL.Dto;
using DAL.UoW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class QueryService
    {
        private readonly UnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public QueryService(UnitOfWork unitOfWork,
            IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        
        public IEnumerable<SummaryObject> GetQuery()
        {
            var users = _unitOfWork.User.GetAll();
            var usersDto = _mapper.Map<List<UserDto>>(users);

            var tasks = _unitOfWork.Task.GetAll();
            var tasksDto = _mapper.Map<List<TaskDto>>(tasks);

            var teams = _unitOfWork.Team.GetAll();
            var teamsDto = _mapper.Map<List<TeamDto>>(teams);

            var projects = _unitOfWork.Project.GetAll();
            var projectsDto = _mapper.Map<List<ProjectDto>>(projects);

            var query1 = from proj in projects
                         select new SummaryObject
                         {
                             ProjectCreatedAt = proj.CreatedAt,
                             ProjectDeadline = proj.Deadline,
                             ProjectDescription = proj.Description,
                             ProjectId = proj.Id,
                             ProjectName = proj.ProjectName,
                             AuthorId = proj.AuthorId,

                             Teams = _mapper.Map<List<TeamDto>>(proj.Teams.Where(t=>t.ProjectId == proj.Id).ToList()),

                             UserTasks = tasksDto.Where(t => t.ProjectId == proj.Id)
                             .Join(usersDto, t => t.PerformerId, u => u.Id,
                             (task, user) => new UserTask
                             {
                                 Task = task,
                                 User = user,
                             }).ToList(),
                         };
            return query1;
        }
        public Dictionary<string, int> GetTasksCountInProject(IEnumerable<SummaryObject> query, int userId)
        {
            var taskForUser = query.Where(p => p.UserTasks.Where(p => p.Task.PerformerId == userId).Any())
                .GroupBy(p => p.ProjectName).ToDictionary(p => p.Key, p => p.Count());
            return taskForUser;
        }
        public IEnumerable<TaskDto> GetListOfTaskWithNameLess45Symbols(IEnumerable<SummaryObject> query, int userId)
        {
            return query.SelectMany(p => p.UserTasks.Where(u => u.User.Id == userId && u.Task.Name.Length < 45), (t, ut) => new TaskDto
            {
                Id = ut.Task.Id,
                Description = ut.Task.Description,
                Name = ut.Task.Name,
                ProjectId = ut.Task.ProjectId,
                State = ut.Task.State,
                CreatedAt = ut.Task.CreatedAt,
                FinishedAt = ut.Task.FinishedAt,
                PerformerId = ut.Task.PerformerId,
            });
        }
        public IEnumerable<object> GetFinishedTaskForUser(IEnumerable<SummaryObject> query, int userId)
        {
            return query
                .SelectMany(t => t.UserTasks
                    .Where(p => p.User.Id == userId && p.Task.FinishedAt != null && p.Task.FinishedAt.Value.Year == 2021),
                    (q, ut) => new
                    {
                        Id = ut.Task.Id,
                        Name = ut.Task.Name
                    });
        }
        public IEnumerable<object> GetUserOlder10Years(IEnumerable<SummaryObject> query)
        {
            return query.SelectMany(t => t.UserTasks, (q, t) => new
            {
                teams = q.Teams,
                User = t.User,
            }).Where(p => DateTime.Now.Year - p.User.BirthDay.Year > 10)
            .GroupBy(p => p.teams);
        }
        public IEnumerable<object> GetUserListByAlfabet(IEnumerable<SummaryObject> query)
        {
            return query.SelectMany(p => p.UserTasks, (q, ut) => new { user = ut.User, task = ut.Task })
                .OrderBy(u => u.user.FirstName).ThenBy(p => p.task.Name.Length)
                .GroupBy(u => u.user);
        }

    }
    public class SummaryObject
    {
        public int ProjectId { get; set; }
        public string ProjectName { get; set; }
        public string ProjectDescription { get; set; }
        public int? AuthorId { get; set; }
        public DateTime ProjectCreatedAt { get; set; }
        public DateTime ProjectDeadline { get; set; }

        public List<TeamDto> Teams { get; set; }
        public List<UserTask> UserTasks { get; set; }

    }
    public class UserTask
    {
        public UserDto User { get; set; }
        public TaskDto Task { get; set; }
    }
}
