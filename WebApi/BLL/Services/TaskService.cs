﻿using AutoMapper;
using BLL.Dto;
using BLL.Interfaces;
using DAL.UoW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tasks = System.Threading.Tasks;

namespace BLL.Services
{
    public class TaskService : ITaskService
    {
        private readonly UnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public TaskService(UnitOfWork unitOfWork,
            IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Tasks.Task CreateAsync(TaskDto entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException();
            }

            var task = _mapper.Map<DAL.Model.Task>(entity);
            await _unitOfWork.Task.CreateAsync(task);
            await _unitOfWork.SaveChangesAsync();

        }

        public async Tasks.Task DeleteAsync(int entityId)
        {
            _unitOfWork.Task.Delete(entityId);
            await _unitOfWork.SaveChangesAsync();
        }

        public IEnumerable<TaskDto> GetAll()
        {
            var taskList = _mapper.Map<IEnumerable<TaskDto>>(_unitOfWork.Task.GetAll());
            return taskList;
        }

        public async Tasks.Task<TaskDto> GetAsync(int id)
        {
            var task = await _unitOfWork.Task.FirstOrDefaultAsync(t => t.Id == id);

            var taskDto = _mapper.Map<TaskDto>(task);

            return taskDto;
        }

        public async Tasks.Task UpdateAsync(TaskDto entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException();
            }

            var task = _mapper.Map<DAL.Model.Task>(entity);

            _unitOfWork.Task.Update(task);
            await _unitOfWork.SaveChangesAsync();
        }
    }
}
