﻿using AutoMapper;
using BLL.Dto;
using BLL.Interfaces;
using DAL.Model;
using DAL.UoW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tasks = System.Threading.Tasks;

namespace BLL.Services
{
    public class TeamService : ITeamService
    {
        private readonly UnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public TeamService(UnitOfWork unitOfWork,
            IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Tasks.Task CreateAsync(TeamDto entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException();
            }

            var team = _mapper.Map<Team>(entity);
            await _unitOfWork.Team.CreateAsync(team);
            await _unitOfWork.SaveChangesAsync();

        }

        public async Tasks.Task DeleteAsync(int entityId)
        {
            _unitOfWork.Team.Delete(entityId);
            await _unitOfWork.SaveChangesAsync();
        }

        public IEnumerable<TeamDto> GetAll()
        {
            var teamList = _mapper.Map<IEnumerable<TeamDto>>(_unitOfWork.Team.GetAll());
            return teamList;
        }

        public async Task<TeamDto> GetAsync(int id)
        {
            var team = await _unitOfWork.Team.FirstOrDefaultAsync(t => t.Id == id);

            var teamDto = _mapper.Map<TeamDto>(team);

            return teamDto;
        }

        public async Tasks.Task UpdateAsync(TeamDto entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException();
            }

            var team = _mapper.Map<Team>(entity);

            _unitOfWork.Team.Update(team);
            await _unitOfWork.SaveChangesAsync();
        }
    }
}
