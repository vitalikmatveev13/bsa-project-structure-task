﻿using AutoMapper;
using BLL.Dto;
using BLL.Interfaces;
using DAL.Model;
using DAL.UoW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tasks = System.Threading.Tasks;

namespace BLL.Services
{
    public class UserService : IUserService
    {
        private readonly UnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public UserService(UnitOfWork unitOfWork,
            IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Tasks.Task CreateAsync(UserDto entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException();
            }

            var user = _mapper.Map<User>(entity);
            _unitOfWork.User.Create(user);
            await _unitOfWork.SaveChangesAsync();

        }

        public async Tasks.Task DeleteAsync(int entityId)
        {
            _unitOfWork.User.Delete(entityId);
            await _unitOfWork.SaveChangesAsync();
        }

        public IEnumerable<UserDto> GetAll()
        {
            var userList = _mapper.Map<IEnumerable<UserDto>>(_unitOfWork.User.GetAll());
            return userList;
        }

        public async Task<UserDto> GetAsync(int id)
        {
            var user = await _unitOfWork.User.FirstOrDefaultAsync(t => t.Id == id);

            var userDto = _mapper.Map<UserDto>(user);

            return userDto;
        }

        public async Tasks.Task UpdateAsync(UserDto entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException();
            }

            var user = _mapper.Map<User>(entity);

            _unitOfWork.User.Update(user);
            await _unitOfWork.SaveChangesAsync();
        }
    }
}
