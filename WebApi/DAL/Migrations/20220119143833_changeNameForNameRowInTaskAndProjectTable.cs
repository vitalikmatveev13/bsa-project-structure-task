﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class changeNameForNameRowInTaskAndProjectTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Name",
                table: "Task",
                newName: "TaskName");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "Project",
                newName: "ProjectName");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "TaskName",
                table: "Task",
                newName: "Name");

            migrationBuilder.RenameColumn(
                name: "ProjectName",
                table: "Project",
                newName: "Name");
        }
    }
}
