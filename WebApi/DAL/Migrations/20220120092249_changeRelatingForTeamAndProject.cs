﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class changeRelatingForTeamAndProject : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TeamId",
                table: "Project");

            migrationBuilder.AddColumn<int>(
                name: "ProjectId",
                table: "Team",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Team_ProjectId",
                table: "Team",
                column: "ProjectId");

            migrationBuilder.AddForeignKey(
                name: "FK_Team_Project_ProjectId",
                table: "Team",
                column: "ProjectId",
                principalTable: "Project",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Team_Project_ProjectId",
                table: "Team");

            migrationBuilder.DropIndex(
                name: "IX_Team_ProjectId",
                table: "Team");

            migrationBuilder.DropColumn(
                name: "ProjectId",
                table: "Team");

            migrationBuilder.AddColumn<int>(
                name: "TeamId",
                table: "Project",
                type: "int",
                nullable: true);
        }
    }
}
