﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model
{
    public class UserConfigurations : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasKey(x => x.Id);
            builder.HasMany(x => x.Tasks).WithOne(x => x.Performer).HasForeignKey(x=>x.PerformerId);
            builder.HasOne(x=>x.Team).WithMany(x=>x.Users).HasForeignKey(x=>x.TeamId);
            builder.Property(x => x.Email).IsRequired();
            builder.HasIndex(x=>x.Email).IsUnique();
            builder.Property(x=>x.FirstName).IsRequired();
            builder.Property(x=>x.LastName).IsRequired();

        }
    }
    public class TaskConfigurations : IEntityTypeConfiguration<DAL.Model.Task>
    {
        public void Configure(EntityTypeBuilder<DAL.Model.Task> builder)
        {
            builder.HasKey(x => x.Id);
            builder.HasOne(t=>t.Performer).WithMany(x=>x.Tasks).HasForeignKey(t=>t.PerformerId);
            builder.Property(x=>x.TaskName).IsRequired();
        }
    }
    public class TeamConfigurations : IEntityTypeConfiguration<Team>
    {
        public void Configure(EntityTypeBuilder<Team> builder)
        {
            builder.HasKey(x => x.Id);
            builder.HasMany(x => x.Users).WithOne(x => x.Team).HasForeignKey(x => x.TeamId);
            builder.Property(x=>x.Name).IsRequired();
            builder.HasOne(p=>p.Project).WithMany(p=>p.Teams).HasForeignKey(p=>p.ProjectId);
        }
    }
    public class ProjectConfigurations : IEntityTypeConfiguration<Project>
    {
        public void Configure(EntityTypeBuilder<Project> builder)
        {
            builder.HasKey(x => x.Id);
            builder.HasOne(x=>x.Author).WithMany(x=>x.Project).HasForeignKey(x=>x.AuthorId);
            builder.Property(x => x.ProjectName);
        }
    }
}
