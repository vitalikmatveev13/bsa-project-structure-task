﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model
{
    public class Project
    {
        public int Id { get; set; }
        public int? AuthorId { get; set; }
        public User Author { get; set; }
        public List<Team> Teams { get; set; }
        public string ProjectName { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
