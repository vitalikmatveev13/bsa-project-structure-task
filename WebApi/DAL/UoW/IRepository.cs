﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DAL.UoW
{
    public interface IRepository<T> where T : class
    {
        IEnumerable<T> GetAll(Expression<Func<T, bool>> filter = null, string includeProperties = null);
        T Get(int id);
        void Create(T item);
        void Update(T item);
        void Delete(int id);

        Task<T> FirstOrDefaultAsync(Expression<Func<T, bool>> filter);
        Task CreateAsync(T item);
    }
}
