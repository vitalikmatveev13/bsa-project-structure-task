﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Model;

namespace DAL.UoW.Repositories
{
    public class TaskRepository : Repository<DAL.Model.Task>
    {
        private readonly AppDbContext _appDbContext;
        public TaskRepository(AppDbContext appDbContext) : base(appDbContext)
        {
            _appDbContext = appDbContext;
        }
    }
}
