﻿using DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.UoW.Repositories
{
    public class TeamRepository : Repository<Team>
    {
        private readonly AppDbContext _appDbContext;
        public TeamRepository(AppDbContext appDbContext) : base(appDbContext)
        {
            _appDbContext = appDbContext;
        }
    }
}
