﻿using DAL.Model;
using DAL.UoW.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.UoW
{
    public class UnitOfWork : IUnitOfWork
    {
        private ProjectRepository _projectRepository;
        private TaskRepository _taskRepository;
        private TeamRepository _teamRepository;
        private UserRepository _userRepository;
        private readonly AppDbContext _dbContext;
        public UnitOfWork(AppDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public ProjectRepository Project
        {
            get 
            { 
                if(_projectRepository == null)
                    return new ProjectRepository(_dbContext);
                return _projectRepository;
            }
        }
        public TaskRepository Task
        {
            get
            {
                if (_taskRepository == null)
                    return new TaskRepository(_dbContext);
                return _taskRepository;
            }
        }
        public TeamRepository Team
        {
            get
            {
                if (_teamRepository == null)
                    return new TeamRepository(_dbContext);
                return _teamRepository;
            }
        }
        public UserRepository User
        {
            get
            {
                if (_userRepository == null)
                    return new UserRepository(_dbContext); ;
                return _userRepository;
            }
        }

        public void SaveChanges()
        {
            _dbContext.SaveChanges();
        }

        public async System.Threading.Tasks.Task SaveChangesAsync()
        {
            await _dbContext.SaveChangesAsync();
        }
    }
}
