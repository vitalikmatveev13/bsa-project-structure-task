﻿using BLL.Dto;
using BLL.Interfaces;
using DAL.UoW;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace WebApi.Controllers
{
    [Route("api/")]
    [ApiController]
    public class ProjectController : ControllerBase
    {
        private readonly IProjectService _projectService;
        public ProjectController(IProjectService projectService)
        {
            _projectService = projectService;
        }

        [HttpGet]
        [Route("projects/{projectId}")]
        public async Task<IActionResult> Get(int projectId)
        {
            var projects = await _projectService.GetAsync(projectId);

            if (projects == null)
            {
                return NotFound();
            }

            return Ok(projects);
        }

        [HttpGet]
        [Route("projects")]
        public IActionResult GetAll()
        {
            var projects = _projectService.GetAll();

            if (projects == null)
            {
                return NotFound();
            }

            return Ok(projects);
        }

        [HttpPost]
        [Route("projects")]
        public async Task<IActionResult> Create(ProjectDto projectDto)
        {
            

            if (projectDto == null)
            {
                return BadRequest();
            }
            await _projectService.CreateAsync(projectDto);
            return Ok(projectDto);
        }

        [HttpDelete]
        [Route("projects/{projectId}")]
        public async Task<IActionResult> Delete(int projectId)
        {
            
            await _projectService.DeleteAsync(projectId);

            return Ok();
        }

        [HttpPut]
        [Route("projects")]
        public async Task<IActionResult> Update(ProjectDto projectDto)
        {


            if (projectDto == null)
            {
                return BadRequest();
            }
            await _projectService.UpdateAsync(projectDto);

            return Ok();
        }
    }
}