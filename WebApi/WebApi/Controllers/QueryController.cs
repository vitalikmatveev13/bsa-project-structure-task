﻿using BLL.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace WebApi.Controllers
{
    [Route("api/")]
    [ApiController]
    public class QueryController : ControllerBase
    {
        private QueryService _queryService;
        IEnumerable<SummaryObject> query;
        public QueryController(QueryService queryService)
        {
            _queryService = queryService;
            query = _queryService.GetQuery();
        }

        [HttpGet]
        [Route("query1/{userId}")]
        public IActionResult GetQuery1(int userId)
        {
            var result = _queryService.GetTasksCountInProject(query,userId);
            return Ok(result);
        }

        [HttpGet]
        [Route("query2/{userId}")]
        public IActionResult GetQuery2(int userId)
        {
            var result = _queryService.GetListOfTaskWithNameLess45Symbols(query,userId);
            return Ok(result);
        }

        [HttpGet]
        [Route("query3/{userId}")]
        public IActionResult GetQuery3(int userId)
        {
            var result = _queryService.GetFinishedTaskForUser(query, userId);
            return Ok(result);
        }

        [HttpGet]
        [Route("query4/")]
        public IActionResult GetQuery4()
        {
            var result = _queryService.GetUserOlder10Years(query);
            return Ok(result);
        }

        [HttpGet]
        [Route("query5/")]
        public IActionResult GetQuery5()
        {
            var result = _queryService.GetUserListByAlfabet(query);
            return Ok(result);
        } 
    }
}
