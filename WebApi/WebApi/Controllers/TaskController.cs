﻿using BLL.Dto;
using BLL.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace WebApi.Controllers
{
    [Route("api/")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        private readonly ITaskService _taskService;
        public TaskController(ITaskService taskService)
        {
            _taskService = taskService;
        }

        [HttpGet]
        [Route("tasks/{taskId}")]
        public async Task<IActionResult> Get(int taskId)
        {
            var taskDto = await _taskService.GetAsync(taskId);

            if (taskDto == null)
            {
                return NotFound();
            }

            return Ok(taskDto);
        }

        [HttpGet]
        [Route("tasks")]
        public IActionResult GetAll()
        {
            var taskDto = _taskService.GetAll();

            if (taskDto == null)
            {
                return NotFound();
            }

            return Ok(taskDto);
        }

        [HttpPost]
        [Route("tasks")]
        public async Task<IActionResult> Create(TaskDto taskDto)
        {


            if (taskDto == null)
            {
                return BadRequest();
            }
            await _taskService.CreateAsync(taskDto);
            return Ok(taskDto);
        }

        [HttpDelete]
        [Route("tasks/{taskId}")]
        public async Task<IActionResult> Delete(int taskId)
        {
            await _taskService.DeleteAsync(taskId);

            return Ok();
        }

        [HttpPut]
        [Route("tasks")]
        public async Task<IActionResult> Update(TaskDto taskDto)
        {


            if (taskDto == null)
            {
                return BadRequest();
            }
            await _taskService.UpdateAsync(taskDto);

            return Ok();
        }
    }
}
