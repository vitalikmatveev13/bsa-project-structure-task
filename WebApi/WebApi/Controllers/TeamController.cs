﻿using BLL.Dto;
using BLL.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace WebApi.Controllers
{
    [Route("api/")]
    [ApiController]
    public class TeamController : ControllerBase
    {
        private readonly ITeamService _teamService;
        public TeamController(ITeamService teamService)
        {
            _teamService = teamService;
        }

        [HttpGet]
        [Route("teams/{teamId}")]
        public async Task<IActionResult> Get(int teamId)
        {
            var teamDto = await _teamService.GetAsync(teamId);

            if (teamDto == null)
            {
                return NotFound();
            }

            return Ok(teamDto);
        }

        [HttpGet]
        [Route("teams")]
        public IActionResult GetAll()
        {
            var teamDto = _teamService.GetAll();

            if (teamDto == null)
            {
                return NotFound();
            }

            return Ok(teamDto);
        }

        [HttpPost]
        [Route("teams")]
        public async Task<IActionResult> Create(TeamDto teamDto)
        {
            if (teamDto == null)
            {
                return BadRequest();
            }
            await _teamService.CreateAsync(teamDto);
            return Ok(teamDto);
        }

        [HttpDelete]
        [Route("teams/{teamId}")]
        public async Task<IActionResult> Delete(int teamId)
        {
            await _teamService.DeleteAsync(teamId);

            return Ok();
        }

        [HttpPut]
        [Route("teams")]
        public async Task<IActionResult> Update(TeamDto teamDto)
        {

            if (teamDto == null)
            {
                return BadRequest();
            }
            await _teamService.UpdateAsync(teamDto);

            return Ok();
        }
    }
}
