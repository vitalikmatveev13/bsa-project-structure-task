﻿using BLL.Dto;
using BLL.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace WebApi.Controllers
{
    [Route("api/")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        [Route("users/{userId}")]
        public async Task<IActionResult> Get(int userId)
        {
            var userDto = await _userService.GetAsync(userId);

            if (userDto == null)
            {
                return NotFound();
            }

            return Ok(userDto);
        }

        [HttpGet]
        [Route("users")]
        public IActionResult GetAll()
        {
            var userDto = _userService.GetAll();

            if (userDto == null)
            {
                return NotFound();
            }

            return Ok(userDto);
        }

        [HttpPost]
        [Route("users")]
        public async Task<IActionResult> Create(UserDto userDto)
        {


            if (userDto == null)
            {
                return BadRequest();
            }
            await _userService.CreateAsync(userDto);
            return Ok(userDto);
        }

        [HttpDelete]
        [Route("users/{userId}")]
        public async Task<IActionResult> Delete(int userId)
        {
            await _userService.DeleteAsync(userId);

            return Ok();
        }

        [HttpPut]
        [Route("users")]
        public async Task<IActionResult> Update(UserDto userDto)
        {


            if (userDto == null)
            {
                return BadRequest();
            }
            await _userService.UpdateAsync(userDto);

            return Ok();
        }
    }
}
