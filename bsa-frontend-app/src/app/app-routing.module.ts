import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { ProjectListComponent } from './components/project-list/project-list.component';
import { TaskListComponent } from './components/task-list/task-list.component';
import { TeamListComponent } from './components/team-list/team-list.component';
import { UserListComponent } from './components/user-list/user-list.component';
import { ExitAboutGuard } from './guards/project.guard';


const routes: Routes = [
  { path: '', component: ProjectListComponent},
  { path: 'projects', component: ProjectListComponent, canDeactivate: [ExitAboutGuard]},
  { path: 'tasks', component: TaskListComponent},
  { path: 'users', component: UserListComponent},
  { path: 'teams', component: TeamListComponent}
  //{ path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
