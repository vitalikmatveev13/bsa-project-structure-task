import { Component } from '@angular/core';
import { pipe } from 'rxjs';
import { Team } from './models/team';
import { HttpIternalService } from './services/http-iternal.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'bsa-frontend-app';
  public teams: Team[];

  constructor(private httpClient: HttpIternalService) {
  }

  public getRequest() {
    var x = this.httpClient.getFullRequest<Team[]>('/api/teams');
    x.subscribe(
      (resp) => {
        this.teams = resp.body
      },
      (error) => console.log(error)
    )
    var y = this.teams;
  }
}
