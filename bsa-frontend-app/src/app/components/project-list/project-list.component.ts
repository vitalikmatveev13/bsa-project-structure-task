import { Component, Input, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Observable } from "rxjs";
import { ComponentCanDeactivate } from "src/app/guards/project.guard";
import { Project } from "src/app/models/project";
import { ProjectService } from "src/app/services/project.service";


@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.scss']
})
export class ProjectListComponent implements OnInit, ComponentCanDeactivate {

  public saved = true;
  public projects: Project[];
  public hasCreateNewTeam = false;
  form: FormGroup;

  constructor(
    private projectService: ProjectService,
  ) { }

  canDeactivate(): boolean | Observable<boolean> {
    if(!this.saved) {
      return confirm("You have unsaved data, are you sure?")
    }
    else {
      return true;
    }
  };

  ngOnInit(): void {
    this.getAllProjects();
    this.form = new FormGroup({
      "projName": new FormControl("", Validators.required),
      "projDescription": new FormControl("", Validators.required),
      "projDeadLine": new FormControl("", Validators.required),
    })
  }

  public getAllProjects(){
    this.projectService.getProjects().subscribe(
      (res) => {
        this.projects = res.body.reverse();
      }
    )
  }

  onChange(status:any) {
    this.saved = status;
  }

  public toggleCreatingForm() {
    this.hasCreateNewTeam = !this.hasCreateNewTeam;
  }

  public createTeam() {
    let newProject: Project = {
      name: this.form.controls["projName"].value,
      createdAt: new Date(),
      description: this.form.controls["projDescription"].value,
      deadLine: this.form.controls["projDeadLine"].value,
    }
    this.projectService.createProject(newProject).subscribe(
      (res) => {
        this.projects.push(res.body)
        this.projects = this.projects.reverse();
      }
    );

    this.toggleCreatingForm();
    this.form.reset();
  }

  public projectWasDeleted(project: Project) {
    this.projects = this.projects.filter(p=>p.id !== project.id)
  }
}
