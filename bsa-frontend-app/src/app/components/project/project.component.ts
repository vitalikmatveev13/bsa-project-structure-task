import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { ComponentCanDeactivate } from 'src/app/guards/project.guard';
import { Project } from '../../models/project';
import { ProjectService } from '../../services/project.service';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.scss']
})
export class ProjectComponent implements OnInit {

  @Input() public project : Project;
  @Input() public saved: boolean;
  @Output() onChange = new EventEmitter<boolean>();
  @Output() deleteElement = new EventEmitter<Project>()

  form: FormGroup;
  public isEdit = false;
  constructor(
    private projectService: ProjectService
  ) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      "newProjectName": new FormControl("", Validators.required),
      "newProjectDescription": new FormControl("", Validators.required)
    }) 
  }

  public isEditProject() {
    this.isEdit = !this.isEdit;
    this.saved = !this.saved;
    this.onChange.emit(this.saved);
  }

  public editProject() {
    var editableProject = this.mapNewData();
    this.projectService.editProject(editableProject).subscribe()
    this.project = editableProject;
    this.isEdit = false;
    this.saved = true;
    this.onChange.emit(this.saved);
  }

  private mapNewData() : Project {
    const editableProject: Project = {
      id: this.project.id,
      name: this.form.controls["newProjectName"].value,
      description: this.form.controls["newProjectDescription"].value,
      authorId: this.project.authorId,
      teamId: this.project.teamId,
      createdAt: this.project.createdAt,
      deadLine: this.project.deadLine
    }
    return editableProject;
  }

  
  public deleteProject() {
    if(confirm("Are you sure?")){
      this.projectService.deleteProject(this.project.id).subscribe();
      this.deleteElement.emit(this.project);

    }
  }
}
