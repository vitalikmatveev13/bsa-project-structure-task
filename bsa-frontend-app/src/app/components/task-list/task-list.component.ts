import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { ComponentCanDeactivate } from 'src/app/guards/project.guard';
import { Task } from 'src/app/models/task';
import { TaskService } from 'src/app/services/task.service';


@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss']
})
export class TaskListComponent implements OnInit {

  public tasks: Task[];
  public hasCreateNewTeam = false;
  form: FormGroup;
  
  constructor(
    private taskService: TaskService
  ) { }

  ngOnInit() {
    this.getAllTasks();
    this.form = new FormGroup({
      "taskName": new FormControl("", Validators.required),
      "taskDescription": new FormControl("", [Validators.required, Validators.minLength(15)])
    })
  }

  public getAllTasks() {
    this.taskService.getTasks().subscribe(
      (res) => {
        this.tasks = res.body.reverse();
      }
    )
  }

  public toggleCreatingForm() {
    this.hasCreateNewTeam = !this.hasCreateNewTeam;
  }

  public createTeam() {
    debugger
    let x = this.form.valid;
    let newTask: Task = {
      name: this.form.controls["taskName"].value,
      description: this.form.controls["taskDescription"].value,
      createdAt: new Date(),
    }
    this.taskService.createTask(newTask).subscribe(
      (res) => {
        this.tasks.push(res.body)
        this.tasks = this.tasks.reverse()
      }
    );
    this.toggleCreatingForm();
    this.form.reset();
  }
  public taskWasDeleted(task: Task) {
    this.tasks = this.tasks.filter(p=>p.id !== task.id)
  }

}
