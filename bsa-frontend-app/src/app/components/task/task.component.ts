import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Task } from 'src/app/models/task';
import { TaskService } from 'src/app/services/task.service';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss']
})
export class TaskComponent implements OnInit {

  @Input() public task: Task;
  @Output() deleteElement = new EventEmitter<Task>()

  public isEdit = false;

  form: FormGroup;
  constructor(
    private taskService: TaskService
  ) { 
  }

  ngOnInit(): void {
    this.form = new FormGroup({
      "newTaskName": new FormControl("", Validators.required),
      "newTaskDescription": new FormControl("", Validators.required)
    })
    var x  = document.getElementById('maticon');
  }

  public isEditTask() {
    this.isEdit = !this.isEdit;
  }

  public editTask() {
    var editedTask = this.mapNewData();
    this.taskService.editTask(editedTask).subscribe();
    this.task = editedTask;
    this.isEdit = false;
  }

  public isFinished() : boolean {
    return this.task.finishedAt !== null;
  }

  private mapNewData() : Task {
    const editableTask: Task = {
      id: this.task.id,
      name: this.form.controls['newTaskName'].value,
      description: this.form.controls['newTaskDescription'].value,
      createdAt: this.task.createdAt,
      finishedAt: this.task.finishedAt,
      performerId: this.task.performerId,
      projectId: this.task.projectId,
      state: this.task.state
    }
    return editableTask;
  }

  public deleteTask() {
    if(confirm("Are you sure?")){
      this.taskService.deleteTask(this.task.id).subscribe();
      this.deleteElement.emit(this.task);
    }
  }

}
