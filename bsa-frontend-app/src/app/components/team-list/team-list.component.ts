import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Team } from '../../models/team';
import { TeamService } from '../../services/team.service';

@Component({
  selector: 'app-team-list',
  templateUrl: './team-list.component.html',
  styleUrls: ['./team-list.component.scss']
})
export class TeamListComponent implements OnInit {

  public teams: Team[]
  public hasCreateNewTeam = false;
  form: FormGroup;

  constructor(
    private teamService: TeamService
  ) { }

  ngOnInit() {
    this.getAllTeams();
    this.form = new FormGroup({
      "teamName": new FormControl("", Validators.required),
    })
  }

  public getAllTeams() {
    this.teamService.getTeams().subscribe(
      (res) => {
        this.teams = res.body.reverse();
      }
    )
  }

  public toggleCreatingForm() {
    this.hasCreateNewTeam = !this.hasCreateNewTeam;
  }

  public createTeam() {
    let newTeam: Team = {
      name: this.form.controls["teamName"].value,
      createdAt: new Date()
    }
    this.teamService.createTeam(newTeam).subscribe(
      (res) => {
        this.teams.push(res.body)
        this.teams = this.teams.reverse();
      }
    );

    this.toggleCreatingForm();
    this.form.reset();
  }

  public teamWasDeleted(team: Team) {
    this.teams = this.teams.filter(p=>p.id !== team.id)
  }

}
