import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Team } from 'src/app/models/team';
import { TeamService } from 'src/app/services/team.service';


@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.scss']
})
export class TeamComponent implements OnInit {

  @Input() public team: Team;
  @Output() deleteElement = new EventEmitter<Team>()

  public isEdit = false;
  form: FormGroup;

  constructor(
    private teamService: TeamService
  ) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      "teamName": new FormControl("", Validators.required),
    })
  }

  public isEditTeam(){
    this.isEdit = !this.isEdit;
  }

  public editTeam() {
    
    let newTeam = this.mapNewData();
    this.teamService.editTeam(newTeam).subscribe();
    this.team = newTeam;
    this.isEdit = false;
  }

  private mapNewData() : Team {
    const editableTeam: Team = {
      id: this.team.id,
      name: this.form.controls['teamName'].value,
      createdAt: this.team.createdAt
    }
    return editableTeam;
  }

  public deleteTeam() {
    if(confirm("Are you sure?")){
      this.teamService.deleteTeam(this.team.id).subscribe();
      this.deleteElement.emit(this.team);
    }
  }
}
