import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

  public users : User[];

  public hasCreateNewTeam = false;
  form: FormGroup;
  
  constructor(
    private userService: UserService
  ) { }

  ngOnInit() {
    this.getAllUsers();

    this.form = new FormGroup({
      "userFirstName": new FormControl("", Validators.required),
      "userLastName": new FormControl("", Validators.required),
      "userEmail": new FormControl("", [Validators.required, Validators.email]),
      "userBirthday": new FormControl("", Validators.required)
    })
  }

  public getAllUsers() {
    this.userService.getUsers().subscribe(
      (res) => {
        this.users = res.body.reverse();
      }
    )
  }

  public toggleCreatingForm() {
    this.hasCreateNewTeam = !this.hasCreateNewTeam;
  }

  public createTeam() {
    debugger
    let x = this.form.valid;
    let newUser: User = {
      firstName: this.form.controls["userFirstName"].value,
      lastName: this.form.controls["userLastName"].value,
      email: this.form.controls["userEmail"].value,
      birthday: this.form.controls["userBirthday"].value,
      registerAt: new Date(),
    }
    this.userService.createUser(newUser).subscribe(
      (res) => {
        this.users.push(res.body)
        this.users = this.users.reverse()
      }
    );
    this.toggleCreatingForm();
    this.form.reset();
  }

  public userWasDeleted(user: User) {
    this.users = this.users.filter(p=>p.id !== user.id)
  }
}
