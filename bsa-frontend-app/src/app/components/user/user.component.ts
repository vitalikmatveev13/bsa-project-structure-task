import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  @Input() public user : User;
  @Output() deleteElement = new EventEmitter<User>()

  form: FormGroup;
  public isEdit = false

  constructor(
    private userService: UserService
    ) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      "userFirstName": new FormControl("", Validators.required),
      "userLastName": new FormControl("", Validators.required),
      "userEmail": new FormControl("",[Validators.required, Validators.email])
    })
  }

  public isEditUser() {
    this.isEdit = !this.isEdit;
  }

  public editUser() {
    var editadUser = this.mapNewData();
    this.userService.editUser(editadUser).subscribe();

    this.user = editadUser;
    this.isEdit = false;
  }

  private mapNewData() : User {
    let editableUser: User = this.user
    editableUser.firstName = this.form.controls["userFirstName"].value;
    editableUser.lastName = this.form.controls["userLastName"].value;
    editableUser.email = this.form.controls["userEmail"].value
    return editableUser;
  }

  public deleteUser(){
    if(confirm("Are you sure?")){
      this.userService.deleteUser(this.user.id).subscribe();
      this.deleteElement.emit(this.user);
    }
  }

}
