import {Directive, ElementRef, HostBinding, HostListener, Input, OnInit, Renderer2} from '@angular/core';
 
@Directive({
    selector: '[task-done]',
})
export class TaskDoneDirective implements OnInit{

    @Input() isFinished = 'false';
    
    constructor(private elementRef: ElementRef, private renderer: Renderer2){
    }
    ngOnInit(): void {
        if(this.isFinished === 'true') {
            let spanEl = this.renderer.createElement('span') as ElementRef;
            this.renderer.addClass(spanEl,'checkmark');
    
            let divLeft = this.renderer.createElement('div') as ElementRef;
            this.renderer.addClass(divLeft,'checkmark_stem');
    
            let divRight = this.renderer.createElement('div') as ElementRef
            this.renderer.addClass(divRight,'checkmark_kick');
    
            this.renderer.appendChild(spanEl,divLeft);
            this.renderer.appendChild(spanEl,divRight);
    
            this.renderer.appendChild(this.elementRef.nativeElement,spanEl)
        }
    }
}