import { TestBed } from '@angular/core/testing';

import { HttpIternalService } from './services/http-iternal.service';

describe('HttpIternalService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HttpIternalService = TestBed.get(HttpIternalService);
    expect(service).toBeTruthy();
  });
});
