export interface Project {
    id?: number,
    authorId?: number,
    teamId?: number,
    name: string,
    description: string,
    deadLine: Date,
    createdAt: Date,

}