export interface User {
    id?: number,
    teamId?: number,
    firstName: string,
    lastName: string,
    email: string,
    registerAt: Date,
    birthday: Date,
}