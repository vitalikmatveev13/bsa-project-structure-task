import { Pipe, PipeTransform } from '@angular/core';
  
@Pipe({
    name: 'uaDate'
})
export class UaDatePipe implements PipeTransform {
  transform(value: Date, args?: any): string {
    let newDate = new Date(value);
    return this.genereteStringDate(newDate);
  }

  private genereteStringDate(date: Date) : string {
    let month = this.getMonth(date.getMonth());
    let day = date.getUTCDate();
    let year = date.getFullYear();
    return `${day} ${month} ${year}`;
  }

  private getMonth(number: number) {
      switch (number) {
        case 0:
            return 'Січень';
            break;
        case 1:
            return 'Лютий';
            break;
        case 2:
            return 'Березнь';
            break;
        case 3:
            return 'Квітень';
            break;
        case 4:
            return 'Травень';
            break;
        case 5:
            return 'Червень';
            break;
        case 6:
            return 'Липень';
            break;
        case 7:
            return 'Серпень';
            break;
        case 8:
            return 'Вересень'
            break;
        case 9:
            return 'Жовтень';
            break;
        case 10:
            return 'Листопад';
            break;
        case 11:
            return 'Грудень'
            break;

        default:
            return null
            break;
      }
  }
}