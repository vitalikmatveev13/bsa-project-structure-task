import { Injectable } from "@angular/core";
import { Project } from "../models/project";
import { HttpIternalService } from "./http-iternal.service";

@Injectable({
    providedIn: 'root'
  })
  export class ProjectService {
    public routePrefix = '/api/projects';
      
    constructor(private httpClient: HttpIternalService) { }

    public getProjects() {
    return this.httpClient.getFullRequest<Project[]>(this.routePrefix);
    }

    public getProjectById(id: number) {
    return this.httpClient.getFullRequest<Project[]>(`${this.routePrefix}/${id}`);  
    }

    public createProject(project: Project) {
        return this.httpClient.postFullRequest<Project>(this.routePrefix, project);
    }

    public deleteProject(projectId: number) {
        return this.httpClient.deleteFullRequest<Project>(`${this.routePrefix}/${projectId}`);
    }

    public editProject(project: Project) {
        return this.httpClient.putFullRequest<Project>(this.routePrefix, project);
    }
  }