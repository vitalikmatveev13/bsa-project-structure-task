import { Injectable } from "@angular/core";
import { Task } from "../models/task";
import { HttpIternalService } from "./http-iternal.service";

@Injectable({
    providedIn: 'root'
  })
  export class TaskService {
      public routePrefix = '/api/tasks';
      
      constructor(private httpClient: HttpIternalService) {
      }

      public getTasks() {
        return this.httpClient.getFullRequest<Task[]>(this.routePrefix);
      }

      public getTaskById(id: number) {
        return this.httpClient.getFullRequest<Task[]>(`${this.routePrefix}/${id}`);  
      }

      public createTask(task: Task) {
          return this.httpClient.postFullRequest<Task>(this.routePrefix, task);
      }

      public deleteTask(taskId: number) {
           return this.httpClient.deleteFullRequest<Task>(`${this.routePrefix}/${taskId}`);
      }

      public editTask(task: Task) {
          return this.httpClient.putFullRequest<Task>(this.routePrefix, task);
      }
  }