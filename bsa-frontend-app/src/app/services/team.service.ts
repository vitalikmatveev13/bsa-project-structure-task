import { Injectable } from "@angular/core";
import { Team } from "../models/team";
import { HttpIternalService } from "./http-iternal.service";

@Injectable({
    providedIn: 'root'
})

export class TeamService {
    public routePrefix = '/api/teams';
    
    constructor(private httpClient: HttpIternalService) {
    }

    public getTeams() {
    return this.httpClient.getFullRequest<Team[]>(this.routePrefix);
    }

    public getTeamById(id: number) {
    return this.httpClient.getFullRequest<Team[]>(`${this.routePrefix}/${id}`);  
    }

    public createTeam(team: Team) {
        return this.httpClient.postFullRequest<Team>(this.routePrefix, team);
    }

    public deleteTeam(teamId: number) {
        return this.httpClient.deleteFullRequest<Team>(`${this.routePrefix}/${teamId}`);
    }

    public editTeam(team: Team) {
        return this.httpClient.putFullRequest<Team>(this.routePrefix, team);
    }
}