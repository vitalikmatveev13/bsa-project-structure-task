import { Injectable } from "@angular/core";
import { User } from "../models/user";
import { HttpIternalService } from "./http-iternal.service";

@Injectable({
    providedIn: 'root'
  })
  export class UserService {
    public routePrefix = '/api/users';

    constructor(private httpClient: HttpIternalService) { }

    public getUsers() {
    return this.httpClient.getFullRequest<User[]>(this.routePrefix);
    }

    public getUserById(id: number) {
    return this.httpClient.getFullRequest<User[]>(`${this.routePrefix}/${id}`);  
    }

    public createUser(user: User) {
        return this.httpClient.postFullRequest<User>(this.routePrefix, user);
    }

    public deleteUser(userId: number) {
        return this.httpClient.deleteFullRequest<User>(`${this.routePrefix}/${userId}`);
    }

    public editUser(user: User) {
        return this.httpClient.putFullRequest<User>(this.routePrefix, user);
    }
}